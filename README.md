# Fully-connected Neural Network for Digit Recognition

Questo progetto è nato per la realizzazioen della tesi: Analisi dell'Efficienza di System on Chip.
In questo repository sono presenti i sorgenti dell'implementazione di una rete neurale interamente connessa che cerca di riconoscere cifre scritte a mano libera. Il dataset utilizzato proviene dal MNIST databse ed è già incluso nel repository.


### Prerequisiti

* GCC (GNU Compiler Collection)
* Makefile


### Compilazione

È sufficiente eseguire il comando `make`.
Se si compila in una macchina che supporta l'architettura ARM NEON: `make ARM=1`


### Supporto architetture SIMD

L'applicazione può supportare le architetture SIMD: ARM NEON, SSE2, AVX2.
Se non si desidera questo supporto, o non si dipone di queste architetture, si può disabilitare il supporto SIMD commentando la riga `#define SIMD_SUPPORT` all'interno del file [mnist-dnn.c](https://bitbucket.org/edobrb/dnn-hpc/src/master/mnist-dnn.c), e ricompilare il progetto.


## Avviare l'addestramento

Prima di eseguire l'addestramento di una rete neurale è necessario compilare un file di configurazione. All'interno del repository ne è gia presente uno: [settings.txt](https://bitbucket.org/edobrb/dnn-hpc/src/master/settings.txt)
Le righe del file di impostazioni hanno i seguenti significati:

```c
hidden_layers_count=2	//numero di livelli nascosti
layer_size=240			//dimensione livello 1
layer_size=120			//dimensione livello 2 ...
h=0.15					//learning rate iniziale
final_h=0.05			//learning rate finale
regularization_force=5	//forza di regolarizzazione L1
backprop_threshold=0.9	//margine di backpropagation
affine_traformation=1	//trasformazione affine {0,1}
r=15					//rotazione in gradi +-r
s=0.15					//ridimensionamento 1+-s
t=2						//spostamento +-t
epochs=300				//numero di epoche
epoch_size=60000		//dimensione dell'epoca
```

Per avviare l'esecuzione dell'applicazione è sufficiente eseguire il comando: `./mnist-dnn < settings.txt` con il file di configurazione desiderato.
Per avviare l'esecuzione dell'applicazione con un grado di parallelismo variabile bisogna specificare nel comando `OMP_NUM_THREADS=x ./mnist-dnn < settings.txt` il valore di `x`.


### Output dell'applicazione

Mentre il sistema di machine learning si sta addestrando viene generato un output come il seguente:
`E 001/300 | Ct: 0.42540 | Ce: 0.13324 | TRAIN: 70.46%, EVAL: 91.77% | 1/h: 6.7 -> 6.7   [31.0 min]`
In ordine significano: epoca attuale / epoche totali, Costo sul training-set, costo sul evaluation-set, successi sul training-set, successi sul evaluation-set, 1/learning rate (valore attuale e valore per la prossima epoca), tempo di training previsto.


## Autori

* **Edoardo Barbieri**
* **Moreno Marzolla** per la libreria [simd_helper.h](https://bitbucket.org/edobrb/dnn-hpc/src/master/simd_helper.h)

Questo programma è distribuito nella speranza che possa essere di aiuto a qualcuno, ma senza nessun tipo di garanzia.
La tesi sviluppata su questo progetto è situata all'indirizzo: [https://amslaurea.unibo.it/16759/](https://amslaurea.unibo.it/16759/)
