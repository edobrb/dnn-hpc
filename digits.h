/****************************************************************************
 *
 * Author:        Barbieri Edoardo
 *
 * Student ID:    0000758593
 *
 * Email:         edoardo.barbieri4(at)studio.unibo.it
 *
 ****************************************************************************/
 
#ifndef __DIGITS_H__
#define __DIGITS_H__

#if _XOPEN_SOURCE < 600
#define _XOPEN_SOURCE 600
#endif

#include <stdlib.h>
#include <stdio.h>
#include "digits-simd.h"

#define DIGIT_SIZE_X 28
#define DIGIT_SIZE_Y 28
#define DIGIT_SIZE (DIGIT_SIZE_X * DIGIT_SIZE_Y)
#define byte unsigned char
#define TRAIN_DIGITS "../digits/train-digits.dat"
#define TRAIN_LABELS "../digits/train-labels.dat"
#define TEST_DIGITS "../digits/test-digits.dat"
#define TEST_LABELS "../digits/test-labels.dat"

typedef struct digit
{
    float* data; //0-1
    int label;   //0-9
} Digit;

int endianSwap(int num)
{
    int swapped = ((num >> 24) & 0xff) | // move byte 3 to byte 0
                  ((num << 8 ) & 0xff0000) | // move byte 1 to byte 2
                  ((num >> 8 ) & 0xff00) | // move byte 2 to byte 1
                  ((num << 24) & 0xff000000); // byte 0 to byte 3
                  return swapped;
}

void easy_print_digit(Digit d)
{
	const float limit1 = 64, limit2 = 128, limit3 = 192;
	for(int x = 0; x < DIGIT_SIZE_X + 2; x++)
	{
		printf("#");
	}
	printf("\n");
	for(int y = 0; y < DIGIT_SIZE_Y; y++)
    {
		printf("#");
        for(int x = 0; x < DIGIT_SIZE_X; x++)
        {
            float val = d.data[x + DIGIT_SIZE_X * y] * 255;
            printf("%c", val <= limit1 ? ' ' : val <= limit2 ? '.' : limit3 <= 192 ? 'o' : 'O');
        }
		printf("#\n");
    }
	for(int x = 0; x < DIGIT_SIZE_X + 2; x++)
	{
		printf("#");
	}
    printf("\n");
}
void print_digit(Digit d)
{
    const float limit1 = 64, limit2 = 128, limit3 = 192;
    printf("Value: %d\n\n", d.label);
    int sx = 0;
    int scan = 1;
    while(scan && sx < DIGIT_SIZE_X)
    {
        for(int y = 0; y < DIGIT_SIZE_Y; y++)
        {
            float val = d.data[sx + DIGIT_SIZE_X * y] * 255;
            if(val > limit1) scan = 0; 
        }
        if(scan) sx++;
    }
    if(sx > 0) sx--;//padding
    
    for(int y = 0; y < DIGIT_SIZE_Y; y++)
    {
        int print = 0;
        for(int x = sx; x < DIGIT_SIZE_X; x++)
        {
            float val = d.data[x + DIGIT_SIZE_X * y] * 255;
            if(val > limit1) print = 1;
        }
        
        for(int x = sx; print && x < DIGIT_SIZE_X; x++)
        {
            float val = d.data[x + DIGIT_SIZE_X * y] * 255;
            printf("%c", val <= limit1 ? ' ' : val <= limit2 ? '.' : limit3 <= 192 ? 'o' : 'O');
        }
        if(print) printf("\n");
    }
    printf("\n");
}

Digit* load_digits(char* digits_filename, char* labels_filename, int* count)
{
    FILE* digits_f = fopen(digits_filename, "rb");
    FILE* labels_f = fopen(labels_filename, "rb");
    
    int magic, digitsCount, rows, cols;
    if(fread(&magic, 1, 4, digits_f) != 4)
		exit(2);
    if(fread(&digitsCount, 1, 4, digits_f) != 4)
		exit(2);
	digitsCount = endianSwap(digitsCount);
    if(fread(&rows, 1, 4, digits_f) != 4)
		exit(2);
	rows = endianSwap (rows);
    if(fread(&cols, 1, 4, digits_f) != 4)
		exit(2);
	cols = endianSwap(cols);
    
    if(fread(&magic, 1, 4, labels_f) != 4)
		exit(2);
    if(fread(&magic, 1, 4, labels_f) != 4) // again digitsCount
		exit(2);
		
    Digit* digits = (Digit*)malloc(sizeof(Digit) * digitsCount);
	
    byte buffer[DIGIT_SIZE];
    for(int i = 0; i < digitsCount; i++)
    {
		digits[i].data = (float*)aligned_malloc(sizeof(float), DIGIT_SIZE);
		
        if(fread(buffer, 1, DIGIT_SIZE, digits_f) != DIGIT_SIZE)
			exit(2);
        for(int k = 0; k < DIGIT_SIZE; k++) 
            digits[i].data[k] = ((float)buffer[k]) / 255.0f;
        if(fread(&digits[i].label, 1, 1, labels_f) != 1)
			exit(2);
    }
    printf("Loaded %d digits [%dx%d]\n", digitsCount, rows, cols);
    fclose(digits_f);
    fclose(labels_f);
    
    *count = digitsCount;
    return digits;
}

typedef struct layer
{
    float* outputs;
	float* biases;
    float* gradients;
    float** weights;
	struct layer* input;
	int size;
} Layer;

float float_rand(float min, float max)
{
    return (((float)rand()) / RAND_MAX) * (max - min) + min;
}
Layer* create_full_connected_layer(Layer* input_layer, int input_layer_size, int size)
{
    Layer* layer = (Layer*)malloc(sizeof(Layer) * 1);
    layer->outputs = (float*)aligned_malloc(sizeof(float), size);
	layer->biases = (float*)aligned_malloc(sizeof(float), size);
    layer->gradients = (float*)aligned_malloc(sizeof(float), size);
    layer->weights = (float**)malloc(sizeof(float*) * size);
	layer->input = input_layer;
    layer->size = size;
    for(int i = 0; i < size; i++)
    {
        layer->outputs[i] = 0;
        layer->gradients[i] = 0;
		layer->biases[i] = float_rand(-0.5f, 0.5f);
        if(input_layer == NULL)
        {
            layer->weights[i] = NULL; //not used
        }
        else
        {
            layer->weights[i] = (float*)aligned_malloc(sizeof(float), input_layer_size);
            for(int k = 0; k < input_layer_size; k++)
            {
                layer->weights[i][k] = float_rand(-0.5f, 0.5f);
            }
        }
    }
    return layer;
}

typedef struct dense_neural_network
{
	Layer* input_layer;
	int hidden_layers_count;
	Layer** hidden_layers;
	Layer* output_layer;
}DenseNeuralNetwork;

DenseNeuralNetwork* create_fully_connected_network(int input_size, int hidden_count, int* hiddens_size, int output_size)
{
	DenseNeuralNetwork* network = (DenseNeuralNetwork*)malloc(sizeof(DenseNeuralNetwork) * 1);
	
	network->input_layer = create_full_connected_layer(NULL, 0, input_size);
	
	free(network->input_layer->gradients);
	network->input_layer->gradients = NULL;
	free(network->input_layer->biases);
	network->input_layer->biases = NULL;
	free(network->input_layer->weights);
	network->input_layer->weights = NULL;
	network->input_layer->input = NULL;
	
	network->hidden_layers_count = hidden_count;
	if(hidden_count > 0)
	{
		network->hidden_layers = (Layer**)malloc(sizeof(Layer*) * hidden_count);
		network->hidden_layers[0] = create_full_connected_layer(network->input_layer, input_size, hiddens_size[0]);
		for (int i = 1; i < hidden_count; i++)
		{
			network->hidden_layers[i] = create_full_connected_layer(network->hidden_layers[i-1], hiddens_size[i-1], hiddens_size[i]);
		}
		network->output_layer = create_full_connected_layer(network->hidden_layers[hidden_count-1], hiddens_size[hidden_count-1], output_size);
	}
	else
	{
		network->output_layer = create_full_connected_layer(network->input_layer, input_size, output_size);
	}
	
	return network;
}

void pulse_layer(Layer* layer)
{
	for (int i = 0; i < layer->size; i++)
	{
		layer->outputs[i] = mul_reduce(layer->weights[i], layer->input->outputs, layer->input->size);
		
		layer->outputs[i] = 1.0f / (1.0f + exp(-layer->outputs[i] - layer->biases[i]));
	}
}
void pulse_layer_parallel(Layer* layer)
{
	#pragma omp parallel for schedule(static, 28)
	for (int i = 0; i < layer->size; i++)
	{
		layer->outputs[i] = mul_reduce(layer->weights[i], layer->input->outputs, layer->input->size);
		
		layer->outputs[i] = 1.0f / (1.0f + exp(-layer->outputs[i] - layer->biases[i]));
	}
}
void pulse_dnn(DenseNeuralNetwork* network, float* data)
{
	//assign input layer value
	memcpy(network->input_layer->outputs, data, sizeof(float) * network->input_layer->size);

	//forward pulse
	if(network->hidden_layers_count > 0)
		pulse_layer(network->hidden_layers[0]);
	for (int i = 1; i < network->hidden_layers_count; i++)
		pulse_layer(network->hidden_layers[i]);
	pulse_layer(network->output_layer);
}
void pulse_dnn_parallel(DenseNeuralNetwork* network, float* data)
{
	//assign input layer value
	memcpy(network->input_layer->outputs, data, sizeof(float) * network->input_layer->size);

	//forward pulse
	if(network->hidden_layers_count > 0)
		pulse_layer_parallel(network->hidden_layers[0]);
	for (int i = 1; i < network->hidden_layers_count; i++)
		pulse_layer_parallel(network->hidden_layers[i]);
	pulse_layer_parallel(network->output_layer);
}
void reset_gradient_dnn(DenseNeuralNetwork* network)
{
	for (int i = 0; i < network->hidden_layers_count; i++)
	{
		memset(network->hidden_layers[i]->gradients, 0, sizeof(float) * network->hidden_layers[i]->size);
	}
	memset(network->output_layer->gradients, 0, sizeof(float) * network->output_layer->size);
}
void backward_layer(Layer* layer, float h, float regulation_force)
{
	//fix my layer gradiend
	sigmoid_derivative(layer->gradients, layer->outputs, layer->size);
	
	//backpropagation
	if(layer->input->gradients != NULL)
		for (int i = 0; i < layer->size; i++)
		{
			mul_accumulate(layer->input->gradients, layer->weights[i], layer->gradients[i], layer->input->size);
		}

	//SGD
	for (int i = 0; i < layer->size; i++)
	{
		compute_gradient_and_update(layer->weights[i], layer->input->outputs, 
		layer->gradients[i], h, regulation_force, layer->input->size);
		layer->biases[i] += layer->gradients[i] * h;
	}
}
void backward_dnn(DenseNeuralNetwork* network, float h, float regulation_force)
{
	//backward pulse
	backward_layer(network->output_layer, h, regulation_force);
	for (int i = network->hidden_layers_count - 1; i >= 0; i--)
		backward_layer(network->hidden_layers[i], h, regulation_force);
}




//source and target sizes must be equals
void clone_layer(Layer* target, Layer* source)
{
	//target, source, size
	memcpy(	target->biases, 
			source->biases, 
			sizeof(float) * source->size);
			
	#pragma omp parallel for schedule(static, 28)
	for(int j = 0; j < source->size; j++)
		memcpy(	target->weights[j], 
				source->weights[j], 
				sizeof(float) * source->input->size);
}
double clone_dnn(DenseNeuralNetwork* target, DenseNeuralNetwork* source)
{
	double start = hpc_gettime();
	for(int i = 0; i < source->hidden_layers_count; i++)
		clone_layer(target->hidden_layers[i], source->hidden_layers[i]);
	
	clone_layer(target->output_layer, source->output_layer);
	return hpc_gettime() - start;
}

double merge_dnn(DenseNeuralNetwork* target, DenseNeuralNetwork** sources, int n)
{
	double start = hpc_gettime();
	#pragma omp parallel for schedule(static, 28)
	for(int i = 0; i < target->output_layer->size; i++)
	{
		float sum = 0;
		for(int s = 0; s < n; s++)
		{
			sum += sources[s]->output_layer->biases[i];
		}
		target->output_layer->biases[i] = sum / n;
	}
	
	#pragma omp parallel for schedule(static, 28)
	for(int j = 0; j < target->output_layer->size; j++)
		for(int i = 0; i < target->output_layer->input->size; i++)
		{
			float sum = 0;
			for(int s = 0; s < n; s++)
			{
				sum += sources[s]->output_layer->weights[j][i];
			}
			target->output_layer->weights[j][i] = sum / n;
		}
		
	
	for(int k = 0; k < target->hidden_layers_count; k++)
	{
		#pragma omp parallel for schedule(static, 28)
		for(int i = 0; i < target->hidden_layers[k]->size; i++)
		{
			float sum = 0;
			for(int s = 0; s < n; s++)
			{
				sum += sources[s]->hidden_layers[k]->biases[i];
			}
			target->hidden_layers[k]->biases[i] = sum / n;
		}
		
		#pragma omp parallel for schedule(static, 28)
		for(int j = 0; j < target->hidden_layers[k]->size; j++)
			for(int i = 0; i < target->hidden_layers[k]->input->size; i++)
			{
				float sum = 0;
				for(int s = 0; s < n; s++)
				{
					sum += sources[s]->hidden_layers[k]->weights[j][i];
				}
				target->hidden_layers[k]->weights[j][i] = sum / n;
			}
	}
	return hpc_gettime() - start;
}



#endif







