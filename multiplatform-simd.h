/****************************************************************************
 *
 * simd_helper.h (original name: qpaths.h)
 *
 * Scalar and SIMD implementation of the q-paths algorithm
 *
 * Copyright (C) 2016 Moreno Marzolla
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * **EDITED by Edoardo Barbieri (08/2018)
 ****************************************************************************/
 
#ifndef QPATHS_H
#define QPATHS_H

#ifndef SERIAL

#define _XOPEN_SOURCE 600

#include <stdlib.h>
#if defined(__SSE2__) || defined(__AVX2__)
#include <immintrin.h>
#elif defined(__ARM_NEON__) 
#include <arm_neon.h>
#else
#error No SIMD extensions available
#endif

#if defined __AVX2__
typedef float vecf __attribute__ ((vector_size(32))) __attribute__((aligned(32)));
typedef int veci __attribute__ ((vector_size(32))) __attribute__((aligned(32)));
//typedef unsigned int vecu __attribute__ ((vector_size(32))) __attribute__((aligned(32)));
#elif defined __SSE2__
typedef float vecf __attribute__ ((vector_size(16))) __attribute__((aligned(16)));
typedef int veci __attribute__ ((vector_size(16))) __attribute__((aligned(16)));
//typedef unsigned int vecu __attribute__ ((vector_size(16))) __attribute__((aligned(16)));
#else
typedef float vecf __attribute__ ((vector_size(16))) __attribute__((aligned(16)));
typedef int veci __attribute__ ((vector_size(16))) __attribute__((aligned(16)));
//typedef unsigned int vecu __attribute__ ((vector_size(16))) __attribute__((aligned(16)));
#endif

#define VLEN (sizeof(vecf)/sizeof(float))
#define INLINE inline __attribute__((always_inline))








//********************************************
//******************OTHER*********************
//********************************************
static INLINE vecf simd_max_f32(vecf a, vecf b) 
{ 
#if defined __AVX2__
    return _mm256_max_ps(a, b);
#elif defined __SSE2__
    return _mm_max_ps(a, b); 
#else
    return (vecf)vmaxq_f32((float32x4_t)a, (float32x4_t)b);
#endif
}

static INLINE vecf simd_min_f32(vecf a, vecf b) 
{ 
#if defined __AVX2__
    return _mm256_min_ps(a, b);
#elif defined __SSE2__
    return _mm_min_ps(a, b); 
#else
    return (vecf)vminq_f32((float32x4_t)a, (float32x4_t)b);
#endif
}






//********************************************
//******************ARITHMETIC****************
//********************************************
static INLINE vecf simd_add_f32(vecf a, vecf b)
{
#if defined __AVX2__
    return _mm256_add_ps(a, b);
#elif defined __SSE2__
    return _mm_add_ps(a, b);
#else
    return (vecf)vaddq_f32((float32x4_t)a, (float32x4_t)b);
#endif
}
static INLINE veci simd_add_i32(veci a, veci b)
{
#if defined __AVX2__
    return (veci)_mm256_add_epi32((__m256i)a, (__m256i)b);
#elif defined __SSE2__
    return (veci)_mm_add_epi32((__m128i)a, (__m128i)b);
#else
    return (veci)vaddq_s32((int32x4_t)a, (int32x4_t)b);
#endif
}
static INLINE vecf simd_sub_f32(vecf a, vecf b)
{
#if defined __AVX2__
    return _mm256_sub_ps(a, b);
#elif defined __SSE2__
    return _mm_sub_ps(a, b);
#else
    return (vecf)vsubq_f32((float32x4_t)a, (float32x4_t)b);
#endif
}
static INLINE veci simd_sub_i32(veci a, veci b)
{
	#if defined __AVX2__
		return (veci)_mm256_sub_epi32((__m256i)a, (__m256i)b);
	#elif defined __SSE2__
		return (veci)_mm_sub_epi32((__m128i)a, (__m128i)b);  
	#else
		return (veci)vsubq_s32((int32x4_t)a, (int32x4_t)b);
	#endif
}
static INLINE vecf simd_mul_f32(vecf a, vecf b)
{
#if defined __AVX2__
    return _mm256_mul_ps(a, b);
#elif defined __SSE2__
    return _mm_mul_ps(a, b);
#else
    return (vecf)vmulq_f32((float32x4_t)a, (float32x4_t)b);
#endif
}
static INLINE vecf simd_mla_f32(vecf a, vecf b, vecf c) // a = a + b * c
{
#if defined __AVX2__
    vecf tmp = _mm256_mul_ps(b, c); 
    return _mm256_add_ps(a, tmp);
#elif defined __SSE2__
    vecf tmp = _mm_mul_ps(b, c); 
    return _mm_add_ps(a, tmp);
#else
    return (vecf)vmlaq_f32((float32x4_t)a, (float32x4_t)b, (float32x4_t)c);
#endif
}






//********************************************
//**************ASSIGNMENT********************
//********************************************
/* Set all lanes of a vecf to the value v */
static INLINE vecf simd_bcast_f32(float v) 
{ 
#if defined __AVX2__
    return _mm256_set1_ps(v);
#elif defined __SSE2__
    return _mm_set_ps1(v);  
#else
    return (vecf)vdupq_n_f32(v);
#endif
}
/* Set all lanes of a veci to the value v */
static INLINE veci simd_bcast_i32(int v) 
{ 
#if defined __AVX2__
    return (veci)_mm256_set1_epi32(v);
#elif defined __SSE2__
    return (veci)_mm_set1_epi32(v);  
#else
    return (veci)vdupq_n_s32(v);
#endif
}







//********************************************
//**************BITWISE***********************
//********************************************
static INLINE veci simd_bitwise_or_i32(veci a, veci b)
{
	#if defined __AVX2__
		return (veci)_mm256_or_si256((__m256i)a, (__m256i)b);
	#elif defined __SSE2__
		return (veci)_mm_or_si128((__m128i)a, (__m128i)b);  
	#else
		return (veci)vorrq_s32((int32x4_t)a, (int32x4_t)b);
	#endif
}
static INLINE veci simd_bitwise_and_i32(veci a, veci b)
{
	#if defined __AVX2__
		return (veci)_mm256_and_si256((__m256i)a, (__m256i)b);
	#elif defined __SSE2__
		return (veci)_mm_and_si128((__m128i)a, (__m128i)b);  
	#else
		return (veci)vandq_s32((int32x4_t)a, (int32x4_t)b);
	#endif
}
static INLINE veci simd_bitwise_not_i32(veci a)
{
	#if defined __AVX2__
		return (veci)_mm256_andnot_si256((__m256i)a, (__m256i)simd_bcast_i32(0xFFFFFFFF));
	#elif defined __SSE2__
		return (veci)_mm_andnot_si128((__m128i)a, (__m128i)simd_bcast_i32(0xFFFFFFFF)); 
	#else
		return (veci)vmvnq_s32((int32x4_t)a);
	#endif
}






//********************************************
//**************CONVERSION********************
//********************************************
static INLINE veci simd_f32_to_i32(vecf a)
{
	#if defined __AVX2__
		return (veci)_mm256_cvttps_epi32(a);
	#elif defined __SSE2__
		return (veci)_mm_cvttps_epi32(a);  
	#else
		return (veci)vcvtq_s32_f32((float32x4_t)a);
	#endif
}
static INLINE vecf simd_i32_to_f32(veci a)
{
	#if defined __AVX2__
		return (vecf)_mm256_cvtepi32_ps((__m256i)a);
	#elif defined __SSE2__
		return (vecf)_mm_cvtepi32_ps((__m128i)a);  
	#else
		return (vecf)vcvtq_f32_s32((int32x4_t)a);
	#endif
}






//********************************************
//**************COMPARSION********************
//********************************************
static INLINE veci simd_greater_f32(vecf a, vecf b)
{
	#if defined __AVX2__
		return (veci)_mm256_cmp_ps(a, b, 30);
	#elif defined __SSE2__
		return (veci)_mm_cmpgt_ps(a, b);  
	#else
		return (veci)vcgtq_f32((float32x4_t)a, (float32x4_t)b);
	#endif
}
static INLINE veci simd_greater_equal_i32(veci a, veci b)
{
	#if defined __AVX2__
		return (veci)simd_bitwise_or_i32((veci)_mm256_cmpgt_epi32((__m256i)a, (__m256i)b), (veci)_mm256_cmpeq_epi32((__m256i)a, (__m256i)b));  
	#elif defined __SSE2__
		return (veci)simd_bitwise_or_i32((veci)_mm_cmpgt_epi32((__m128i)a, (__m128i)b), (veci)_mm_cmpeq_epi32((__m128i)a, (__m128i)b));  
	#else
		return (veci)vcgeq_s32((int32x4_t)a, (int32x4_t)b);
	#endif
}
static INLINE veci simd_less_i32(veci a, veci b)
{
	#if defined __AVX2__
		return (veci)simd_bitwise_not_i32(simd_greater_equal_i32(a, b));  
	#elif defined __SSE2__
		return (veci)simd_bitwise_not_i32(simd_greater_equal_i32(a, b));  
	#else
		return (veci)vcltq_s32((int32x4_t)a, (int32x4_t)b);
	#endif
}






//********************************************
//***************COMPLEX FUNCTION*************
//********************************************
veci simd_floor_f32(vecf x)
{
	//return x > 0 ? (int)x : ((int)x) - 1
	
	vecf __zerof = simd_bcast_f32(0.0f); 
	veci __onei = simd_bcast_i32(1); 
	
	veci cmp_Xo = simd_greater_f32(x, __zerof);
	
	veci Xoi = simd_f32_to_i32(x);
	
	veci not_cmp_Xo = simd_bitwise_not_i32(cmp_Xo);
	veci Xoi_minus1 = simd_sub_i32(Xoi, __onei);
	
	veci firstCond = simd_bitwise_and_i32(cmp_Xo, Xoi);
	veci secondCond = simd_bitwise_and_i32(not_cmp_Xo, Xoi_minus1);
	
	return simd_bitwise_or_i32(firstCond, secondCond);
			
}







//********************************************
//***************MEMORY FUNCTIONS*************
//********************************************
void* aligned_malloc(size_t elem_size, size_t size)
{
    void* result;
    if(posix_memalign(&result, elem_size * VLEN, elem_size * size) != 0)
	{
		exit(1);
	}
    return result;
}
/* Unaligned load from memory of a vector(float) */
static INLINE vecf simd_loadu_f32(const float *v) 
{ 
#if defined __AVX2__
    return _mm256_loadu_ps(v);
#elif defined __SSE2__
    return _mm_loadu_ps(v);  
#else
    return (vecf)vld1q_f32(v);
#endif
}
/* Unaligned load from memory of a vector(int) */
static INLINE veci simd_loadu_i32(const veci *v) 
{ 
#if defined __AVX2__
    return (veci)_mm256_loadu_si256((const __m256i*)v);
#elif defined __SSE2__
    return (veci)_mm_loadu_si128((const __m128i*)v);  
#else
    return (veci)vld1q_s32((const int*)v);
#endif
}
/* Aligned load from memory of a vector(float) */
static INLINE vecf simd_load_f32(const float *v) 
{ 
    return *((vecf*)v);
}
static INLINE veci simd_load_i32(const float* v)
{
    return *((veci*)v);
}
/* Unaligned store to memory of a vector(float) */
static INLINE void simd_storeu_f32(float *l, vecf v) 
{ 
#if defined __AVX2__
    _mm256_storeu_ps(l, v);
#elif defined __SSE2__
    _mm_storeu_ps(l, v); 
#else
    vst1q_f32(l, (float32x4_t)v);
#endif
}
/* Unaligned store to memory of a vector(int) */
static INLINE void simd_storeu_i32(veci *l, veci v) 
{ 
#if defined __AVX2__
    _mm256_storeu_si256((__m256i*)l, (__m256i)v);
#elif defined __SSE2__
    _mm_storeu_si128((__m128i*)l, (__m128i)v); 
#else
    vst1q_s32((int*)l, (int32x4_t)v);
#endif
}
/* Aligned store of a vector(float) */
static INLINE void simd_store_f32(float *l, vecf v) 
{ 
#if defined __AVX2__ 
    _mm256_store_ps(l, v);
#elif defined __SSE2__
    _mm_store_ps(l, v); 
#else
    *((vecf*)l) = v;
#endif
}


#endif

#endif
