/****************************************************************************
 *
 * Author:        Barbieri Edoardo
 *
 * Student ID:    0000758593
 *
 * Email:         edoardo.barbieri4(at)studio.unibo.it
 *
 ****************************************************************************/
 
#ifndef __TRANSFORM_HELPER__
#define __TRANSFORM_HELPER__

#include "multiplatform-simd.h"

float pixelOrBackground(float* image, int x, int y, int width, int height, float background)
{
	 if (x >= 0 && x < width && y >= 0 && y < height) return image[y * width + x];
     else return background;
}

vecf pixelOrBackground_v(float* image, veci x, veci y, vecf background, veci zero, veci width, veci height)
{
	veci cond = simd_bitwise_and_i32(
		simd_bitwise_and_i32(
			simd_greater_equal_i32(x, zero),
			simd_greater_equal_i32(y, zero)),
		simd_bitwise_and_i32(
			simd_less_i32(x, width),
			simd_less_i32(y, height)));
	
	for(int j = 0; j < VLEN; j++)
	{
		if (cond[j]) background[j] = image[y[j] * width[j] + x[j]];
	}
	return background;
}

double affine_transform_simd(float* image, int width, int height, float background, float degree, float Sx, float Sy, float Tx, float Ty, float RCx, float RCy, float* res_image, int res_width, int res_height)
{  
	double start = hpc_gettime();
	double alpha = M_PI * degree / 180.0f;            
	
	float Xa = (1.0f / Sx) * cos(alpha), Xb = (1.0f / Sx) * sin(alpha);
	float Ya = -(1.0f / Sy) * sin(alpha), Yb = (1.0f / Sy) * cos(alpha);
	float Cx = RCx * width, Cy = RCy * height;
	float FTx = (RCx - 0.5) * width + 0.5 * res_width + Tx, FTy = (RCy - 0.5) * height + 0.5 * res_height + Ty;
	
	vecf Xa_v = simd_bcast_f32(Xa);
	vecf Xb_v = simd_bcast_f32(Xb);
	vecf Ya_v = simd_bcast_f32(Ya);
	vecf Yb_v = simd_bcast_f32(Yb);
	vecf Cx_v = simd_bcast_f32(Cx);
	vecf Cy_v = simd_bcast_f32(Cy);
	vecf FTx_v = simd_bcast_f32(FTx);
	vecf FTy_v = simd_bcast_f32(FTy);
	veci onei = simd_bcast_i32(1);
	veci zeroi = simd_bcast_i32(0);
	vecf index = simd_bcast_f32(0);
	vecf background_v = simd_bcast_f32(background);
	veci width_v = simd_bcast_i32(width);
	veci height_v = simd_bcast_i32(height);
	
	for(int j = 1; j < VLEN; j++)
	{
		index[j] += j;
	}
	
	for(int y = 0; y < res_height; y++)
	{
		int x, xl = res_width + 1 - VLEN;
		for(x = 0; x < xl; x += VLEN)
		{
			vecf y_v = simd_bcast_f32((float)y);
			vecf x_v = simd_bcast_f32((float)x);
			x_v = simd_add_f32(x_v, index);
			
			vecf a = simd_sub_f32(x_v, FTx_v);
			vecf b = simd_sub_f32(y_v, FTy_v);
			vecf Xo = simd_add_f32(simd_mla_f32(Cx_v, Xb_v, b), simd_mul_f32(Xa_v, a));
			vecf Yo = simd_add_f32(simd_mla_f32(Cy_v, Yb_v, b), simd_mul_f32(Ya_v, a));
			
			//linear interpolation
			veci x1 = simd_floor_f32(Xo);
			veci y1 = simd_floor_f32(Yo);
			veci x2 = simd_add_i32(x1, onei);
			veci y2 = simd_add_i32(y1, onei);

			vecf x1f = simd_i32_to_f32(x1);
			vecf x2f = simd_i32_to_f32(x2);
			vecf y1f = simd_i32_to_f32(y1);
			vecf y2f = simd_i32_to_f32(y2);
			
			vecf wA = simd_mul_f32(simd_sub_f32(y2f, Yo), simd_sub_f32(x2f, Xo));
			vecf wB = simd_mul_f32(simd_sub_f32(y2f, Yo), simd_sub_f32(Xo, x1f));
			vecf wC = simd_mul_f32(simd_sub_f32(Yo, y1f), simd_sub_f32(x2f, Xo));
			vecf wD = simd_mul_f32(simd_sub_f32(Yo, y1f), simd_sub_f32(Xo, x1f));
			
			vecf wAp = pixelOrBackground_v(image, x1, y1, background_v, zeroi, width_v, height_v);
			vecf wBp = pixelOrBackground_v(image, simd_add_i32(x1, onei), y1, background_v, zeroi, width_v, height_v);
			vecf wCp = pixelOrBackground_v(image, x1, simd_add_i32(y1, onei), background_v, zeroi, width_v, height_v);
			vecf wDp = pixelOrBackground_v(image, simd_add_i32(x1, onei), simd_add_i32(y1, onei), background_v, zeroi, width_v, height_v);
			
			vecf value = 
			simd_add_f32(
				simd_add_f32(
					simd_mul_f32(wA, wAp),
					simd_mul_f32(wB, wBp)),
				simd_add_f32(
					simd_mul_f32(wC, wCp),
					simd_mul_f32(wD, wDp)));

			simd_store_f32(&(res_image[y * res_width + x]), value);
		}
		
		for(;x < res_width; x++)
		{
			float a = x - FTx;
			float b = y - FTy;
			
			float Xo = Xa * a + Xb * b + Cx;
			float Yo = Ya * a + Yb * b + Cy;
			
			//linear interpolation
			int x1 = (int)(Xo > 0 ? Xo : (Xo - 1));
			int y1 = (int)(Yo > 0 ? Yo : (Yo - 1));
			int x2 = x1 + 1, y2 = y1 + 1;
			
			float wA = (y2 - Yo) * (x2 - Xo);
            float wB = (y2 - Yo) * (Xo - x1);
            float wC = (Yo - y1) * (x2 - Xo);
            float wD = (Yo - y1) * (Xo - x1);
			
			float value = 
			wA * pixelOrBackground(image, x1, y1, width, height, background) + 
			wB * pixelOrBackground(image, x1 + 1, y1, width, height, background) + 
			wC * pixelOrBackground(image, x1, y1 + 1, width, height, background) + 
			wD * pixelOrBackground(image, x1 + 1, y1 + 1, width, height, background);
			
			res_image[y * res_width + x] = value;
		}
	}
	return hpc_gettime() - start;
}

double affine_transform_simd_omp(float* image, int width, int height, float background, float degree, float Sx, float Sy, float Tx, float Ty, float RCx, float RCy, float* res_image, int res_width, int res_height)
{
	double start = hpc_gettime();
	double alpha = M_PI * degree / 180.0f;            
	
	float Xa = (1.0f / Sx) * cos(alpha), Xb = (1.0f / Sx) * sin(alpha);
	float Ya = -(1.0f / Sy) * sin(alpha), Yb = (1.0f / Sy) * cos(alpha);
	float Cx = RCx * width, Cy = RCy * height;
	float FTx = (RCx - 0.5) * width + 0.5 * res_width + Tx, FTy = (RCy - 0.5) * height + 0.5 * res_height + Ty;
	
	vecf Xa_v = simd_bcast_f32(Xa);
	vecf Xb_v = simd_bcast_f32(Xb);
	vecf Ya_v = simd_bcast_f32(Ya);
	vecf Yb_v = simd_bcast_f32(Yb);
	vecf Cx_v = simd_bcast_f32(Cx);
	vecf Cy_v = simd_bcast_f32(Cy);
	vecf FTx_v = simd_bcast_f32(FTx);
	vecf FTy_v = simd_bcast_f32(FTy);
	veci onei = simd_bcast_i32(1);
	veci zeroi = simd_bcast_i32(0);
	vecf index = simd_bcast_f32(0);
	vecf background_v = simd_bcast_f32(background);
	veci width_v = simd_bcast_i32(width);
	veci height_v = simd_bcast_i32(height);
	
	for(int j = 1; j < VLEN; j++)
	{
		index[j] += j;
	}
	
	#pragma omp parallel for schedule(static, 1)
	for(int y = 0; y < res_height; y++)
	{
		int x, xl = res_width + 1 - VLEN;
		for(x = 0; x < xl; x += VLEN)
		{
			vecf y_v = simd_bcast_f32((float)y);
			vecf x_v = simd_bcast_f32((float)x);
			x_v = simd_add_f32(x_v, index);
			
			vecf a = simd_sub_f32(x_v, FTx_v);
			vecf b = simd_sub_f32(y_v, FTy_v);
			vecf Xo = simd_add_f32(simd_mla_f32(Cx_v, Xb_v, b), simd_mul_f32(Xa_v, a));
			vecf Yo = simd_add_f32(simd_mla_f32(Cy_v, Yb_v, b), simd_mul_f32(Ya_v, a));
			
			//linear interpolation
			veci x1 = simd_floor_f32(Xo);
			veci y1 = simd_floor_f32(Yo);
			veci x2 = simd_add_i32(x1, onei);
			veci y2 = simd_add_i32(y1, onei);

			vecf x1f = simd_i32_to_f32(x1);
			vecf x2f = simd_i32_to_f32(x2);
			vecf y1f = simd_i32_to_f32(y1);
			vecf y2f = simd_i32_to_f32(y2);
			
			vecf wA = simd_mul_f32(simd_sub_f32(y2f, Yo), simd_sub_f32(x2f, Xo));
			vecf wB = simd_mul_f32(simd_sub_f32(y2f, Yo), simd_sub_f32(Xo, x1f));
			vecf wC = simd_mul_f32(simd_sub_f32(Yo, y1f), simd_sub_f32(x2f, Xo));
			vecf wD = simd_mul_f32(simd_sub_f32(Yo, y1f), simd_sub_f32(Xo, x1f));
			
			vecf wAp = pixelOrBackground_v(image, x1, y1, background_v, zeroi, width_v, height_v);
			vecf wBp = pixelOrBackground_v(image, simd_add_i32(x1, onei), y1, background_v, zeroi, width_v, height_v);
			vecf wCp = pixelOrBackground_v(image, x1, simd_add_i32(y1, onei), background_v, zeroi, width_v, height_v);
			vecf wDp = pixelOrBackground_v(image, simd_add_i32(x1, onei), simd_add_i32(y1, onei), background_v, zeroi, width_v, height_v);
			
			vecf value = 
			simd_add_f32(
				simd_add_f32(
					simd_mul_f32(wA, wAp),
					simd_mul_f32(wB, wBp)),
				simd_add_f32(
					simd_mul_f32(wC, wCp),
					simd_mul_f32(wD, wDp)));

			simd_store_f32(&(res_image[y * res_width + x]), value);
		}
		
		for(;x < res_width; x++)
		{
			float a = x - FTx;
			float b = y - FTy;
			
			float Xo = Xa * a + Xb * b + Cx;
			float Yo = Ya * a + Yb * b + Cy;
			
			//linear interpolation
			int x1 = (int)(Xo > 0 ? Xo : (Xo - 1));
			int y1 = (int)(Yo > 0 ? Yo : (Yo - 1));
			int x2 = x1 + 1, y2 = y1 + 1;
			
			float wA = (y2 - Yo) * (x2 - Xo);
            float wB = (y2 - Yo) * (Xo - x1);
            float wC = (Yo - y1) * (x2 - Xo);
            float wD = (Yo - y1) * (Xo - x1);
			
			float value = 
			wA * pixelOrBackground(image, x1, y1, width, height, background) + 
			wB * pixelOrBackground(image, x1 + 1, y1, width, height, background) + 
			wC * pixelOrBackground(image, x1, y1 + 1, width, height, background) + 
			wD * pixelOrBackground(image, x1 + 1, y1 + 1, width, height, background);
			
			res_image[y * res_width + x] = value;
		}
	}
	return hpc_gettime() - start;
}

double affine_transform_omp(float* image, int width, int height, float background, float degree, float Sx, float Sy, float Tx, float Ty, float RCx, float RCy, float* res_image, int res_width, int res_height)
{
	double start = hpc_gettime();
	double alpha = M_PI * degree / 180.0f;            
	
	float Xa = (1.0f / Sx) * cos(alpha), Xb = (1.0f / Sx) * sin(alpha);
	float Ya = -(1.0f / Sy) * sin(alpha), Yb = (1.0f / Sy) * cos(alpha);
	float Cx = RCx * width, Cy = RCy * height;
	float FTx = (RCx - 0.5) * width + 0.5 * res_width + Tx, FTy = (RCy - 0.5) * height + 0.5 * res_height + Ty;
	
	
	#pragma omp parallel for schedule(static, 1)
	for(int y = 0; y < res_height; y++)
	{
		for(int x = 0; x < res_width; x++)
		{
			float a = x - FTx;
			float b = y - FTy;
			
			float Xo = Xa * a + Xb * b + Cx;
			float Yo = Ya * a + Yb * b + Cy;
			
			//linear interpolation
			int x1 = (int)(Xo > 0 ? Xo : (Xo - 1));
			int y1 = (int)(Yo > 0 ? Yo : (Yo - 1));
			int x2 = x1 + 1, y2 = y1 + 1;
			
			float wA = (y2 - Yo) * (x2 - Xo);
            float wB = (y2 - Yo) * (Xo - x1);
            float wC = (Yo - y1) * (x2 - Xo);
            float wD = (Yo - y1) * (Xo - x1);
			
			float value = 
			wA * pixelOrBackground(image, x1, y1, width, height, background) + 
			wB * pixelOrBackground(image, x1 + 1, y1, width, height, background) + 
			wC * pixelOrBackground(image, x1, y1 + 1, width, height, background) + 
			wD * pixelOrBackground(image, x1 + 1, y1 + 1, width, height, background); 
			
			res_image[y * res_width + x] = value;
		}
	}
	return hpc_gettime() - start;
}




#endif