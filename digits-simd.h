/****************************************************************************
 *
 * Author:        Barbieri Edoardo
 *
 * Student ID:    0000758593
 *
 * Email:         edoardo.barbieri4(at)studio.unibo.it
 *
 ****************************************************************************/
 
#ifndef __DIGITS_SIMD_H__
#define __DIGITS_SIMD_H__

#include "multiplatform-simd.h"

//weight += h * (input * gradient - weight * regulation_force)
void compute_gradient_and_update(float* weight, float* input, float gradient, float h, float regulation_force, int n)
{
	#ifdef SIMD_SUPPORT
	vecf weight_v, input_v, res;
    vecf regulation_force_v = simd_bcast_f32(regulation_force);
	vecf h_v = simd_bcast_f32(h);
	vecf gradient_v = simd_bcast_f32(gradient);
    
    int i, l = n + 1 - VLEN;
    for(i = 0; i < l; i += VLEN)
    {
        weight_v = simd_load_f32(&weight[i]);
        input_v = simd_load_f32(&input[i]);
		
		res = simd_add_f32(simd_mul_f32(weight_v, regulation_force_v), simd_mul_f32(simd_mul_f32(input_v,gradient_v),h_v));
        simd_store_f32(&weight[i], res);
    }
    
    for(; i < n; i ++)
    {
        weight[i] = weight[i] * regulation_force +h * (input[i] * gradient);
    }
	#else
	for(int i = 0; i < n; i ++)
    {
        weight[i] = weight[i] * regulation_force +h * (input[i] * gradient);
    }
	#endif
}

// a = a + b * c
void mul_accumulate(float* a, float* b, float c, int n) 
{
	#ifdef SIMD_SUPPORT
    vecf a4t;
    vecf b4t;
    vecf c4t = simd_bcast_f32(c);
    
    int i, l = n + 1 - VLEN;
    for(i = 0; i < l; i += VLEN)
    {
        a4t = simd_load_f32(&a[i]); //load
        b4t = simd_load_f32(&b[i]); //load
        
        a4t = simd_mla_f32(a4t, b4t, c4t); // a = a + b * c;
        simd_store_f32(&a[i], a4t); //store
    }
    
    for(; i < n; i ++)
    {
        a[i] += b[i] * c;
    }
	#else
	for(int i = 0; i < n; i ++)
    {
        a[i] += b[i] * c;
    }
	#endif
}

//SUM[b*c]
float mul_reduce(float* b, float* c, int n) 
{
	#ifdef SIMD_SUPPORT
    vecf a4t = simd_bcast_f32(0);
    vecf b4t;
    vecf c4t;
    int i, l = n + 1 - VLEN;
    for(i = 0; i < l; i += VLEN)
    {
        b4t = simd_load_f32(&b[i]); //load
        c4t = simd_load_f32(&c[i]); //load
        
        a4t = simd_mla_f32(a4t, b4t, c4t); // a = a + b * c;
    }
    for(; i < n; i ++)
    {
        a4t[0] += b[i] * c[i];
    }
    
    for(int j = 1; j < VLEN; j++)
    {
        a4t[0] += a4t[j];
    }
    return a4t[0];
	#else
	float a = 0;
	for(int i = 0; i < n; i ++)
    {
        a += b[i] * c[i];
    }
	return a;
	#endif
}

// a = a * b * (1 - b)
void sigmoid_derivative(float* a, float* b, int n) 
{
	#ifdef SIMD_SUPPORT
    vecf a4t;
    vecf b4t;
    vecf tmp;
    vecf one = simd_bcast_f32(1);
    
    int i;
    for(i = 0; i < n - 3; i += 4)
    {
        a4t = simd_load_f32(&a[i]); //load
        b4t = simd_load_f32(&b[i]); //load
        
        tmp = simd_mul_f32(a4t, b4t);  //a * b
        b4t = simd_sub_f32(one, b4t);  //1 - b
        
        a4t = simd_mul_f32(tmp, b4t); // a = (a * b) * (1 - b);
        simd_store_f32(&a[i], a4t); //store
    }
    
    for(; i < n; i ++)
    {
        a[i] = b[i] * (1.0f - b[i]) * a[i];
    }
	#else
	for(int i = 0; i < n; i ++)
    {
        a[i] = b[i] * (1.0f - b[i]) * a[i];
    }
	#endif
}

#endif