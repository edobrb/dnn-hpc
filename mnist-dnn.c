/****************************************************************************
 *
 * Author:        Barbieri Edoardo
 *
 * Student ID:    0000758593
 *
 * Email:         edoardo.barbieri4(at)studio.unibo.it
 *
 ****************************************************************************/
 
#define _XOPEN_SOURCE 600
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <time.h>
#include <pthread.h>

#define SIMD_SUPPORT //define simd support

#include "hpc.h"
#include "digits.h"
#include "transform-helper.h"

#define INPUT_LAYER_SIZE DIGIT_SIZE
#define OUTPUT_LAYER_SIZE 10

int main(int argc, char* argv[])
{
    srand(0);
	
	int hidden_layers_count;
	int hidden_layers_size[10] = { 240, 120 };
	float h, start_h = 0.15; 
	float final_h = 0.015; 
	float lamda = 5;
	float regulation_force;
	float backprop_threshold = 0.85f;
	float backprop_threshold_inv = 1 - backprop_threshold;
	int affine_traformation = 1;
	float r = 15, s = 0.15, t = 2;
	int epochs = 200;
	int epoch_size = 40000;
	int graph = 0;
	
	if(fscanf(stdin, "hidden_layers_count=%d\n", &hidden_layers_count) != 1)
	{
		printf("Error reading hidden_layers_count\n");
		exit(1);
	}
	for(int i = 0; i < hidden_layers_count; i++)
	{
		if(fscanf(stdin, "layer_size=%d\n", &(hidden_layers_size[i])) != 1)
		{
			printf("Error reading layer_size\n");
			exit(1);
		}
	}
	if(fscanf(stdin, "h=%f\nfinal_h=%f\nregularization_force=%f\nbackprop_threshold=%f\naffine_traformation=%d\nr=%f\ns=%f\nt=%f\nepochs=%d\nepoch_size=%d\ngraph=%d", &start_h, &final_h, &lamda, &backprop_threshold, &affine_traformation, &r, &s, &t, &epochs, &epoch_size, &graph) < 10)
	{
		printf("Error reading other parameters\n");
		exit(1);
	}
	h = start_h;
	
	//create dnn
	DenseNeuralNetwork* network = create_fully_connected_network(INPUT_LAYER_SIZE, hidden_layers_count, hidden_layers_size, OUTPUT_LAYER_SIZE);	
	
	
	//create a clone foreach thread
	int parallel_networks = omp_get_max_threads();
	DenseNeuralNetwork** nets = (DenseNeuralNetwork**)malloc(sizeof(DenseNeuralNetwork*) * parallel_networks);
	for(int i = 0; i < parallel_networks; i++)
	{
		nets[i] = create_fully_connected_network(INPUT_LAYER_SIZE, hidden_layers_count, hidden_layers_size, OUTPUT_LAYER_SIZE);
	}
	
	
	//buffers for digit trasformation
	float** transformed_digit = (float**)aligned_malloc(sizeof(float*), parallel_networks);
	for(int i = 0; i < parallel_networks; i++)
		transformed_digit[i] = (float*)aligned_malloc(sizeof(float), DIGIT_SIZE);
	
	
	//run time variables
	int remaining_exec = 0; 
	int passed_test, passed_train, backprop, total_exed;
	double cost = 0;
	
	
	//print log
	printf("NETOWRK CONFIGURATION:\n");
	printf("DNN %d-",INPUT_LAYER_SIZE); 
	for(int i = 0; i < hidden_layers_count; i++) 
		printf("%d-",hidden_layers_size[i]);
	printf("%d\n",OUTPUT_LAYER_SIZE);
	printf("h: 1 / %.0f | final h: 1 / %.0f\nregularization force: %.1f\nbackprop threshold: [%f, %f]\n", 
	1 / h, 1 / final_h, lamda, backprop_threshold_inv, backprop_threshold);
	if(affine_traformation) printf("rotation variation: %f\nsize variation: %f\n", r, s);
	else printf("no affine transform\n");
	printf("epoch size: %d\ntotal epochs: %d\n\n", epoch_size, epochs);
    
	
	//load training and test sets
    int digitsCount, testDigitsCount;
    Digit* digits = load_digits(TRAIN_DIGITS, TRAIN_LABELS, &digitsCount);
    Digit* testDigits = load_digits(TEST_DIGITS, TEST_LABELS, &testDigitsCount);
	regulation_force = (1 - (h * lamda) / digitsCount);
	
	//time variables
    double subtstart, totalTime = 0, totalTimeAffine = 0, totalTimeCloneMerge = 0;
	
	
	//start training
	printf("\nTraining started...\n");
    for(int e = 0; e < epochs; e++)
    {
		//shuffle training set
		for(int i = 0; i < digitsCount / 2; i++)
		{
			int a = rand() % digitsCount;
			int b = rand() % digitsCount;
			float* tmp_data = digits[a].data;
			int tmp_label = digits[a].label;
			digits[a].data = digits[b].data;
			digits[a].label = digits[b].label;
			digits[b].data = tmp_data;
			digits[b].label = tmp_label;
		} 
		subtstart = hpc_gettime();
		
		
		//clone DNNs
		for(int i = 0; i < parallel_networks; i++)
			totalTimeCloneMerge += clone_dnn(nets[i], network);		
	
	
		passed_train = 0;
		int nt = omp_get_max_threads();
		int imagePortion = digitsCount / nt;
		int kepp_running = 1;
		double subTotalTimeAffine = 0;
		#pragma omp parallel reduction(+:passed_train, backprop, total_exed, subTotalTimeAffine, cost) num_threads(nt) default(shared) firstprivate(remaining_exec) private(network)
		{
			int tid = omp_get_thread_num();
			int portion = (epoch_size + remaining_exec) / nt;
			int remainder = tid == nt - 1 ? (epoch_size + remaining_exec) % nt : 0;
			int remainderImage = tid == nt - 1 ? digitsCount % nt : 0;
			network = nets[tid];

			for(int a = tid * portion; a < (tid + 1) * portion + remainder && (kepp_running || (e == epochs - 1)); a++)
			{
				//image to read
				int d = (rand() % (imagePortion + remainderImage)) + tid * imagePortion;

				if(affine_traformation)
				{
					//transform [d] image
					subTotalTimeAffine += affine_transform_simd(digits[d].data, DIGIT_SIZE_X, DIGIT_SIZE_Y, 0,
					float_rand(-r, r), float_rand(-s, s) + 1, float_rand(-s, s) + 1, float_rand(-t, t), float_rand(-t, t), 0.5, 0.5, 
					transformed_digit[tid], DIGIT_SIZE_X, DIGIT_SIZE_Y);
				}
				else
				{
					transformed_digit[tid] = digits[d].data;
				}

				//feedforward image
				pulse_dnn(network, transformed_digit[tid]);
				reset_gradient_dnn(network);
				
				//check if network is wrong
				int max = 0, to_backward = 0;
				for (int i = 0; i < OUTPUT_LAYER_SIZE; i++)
				{
					//the perceptron output's is a number between ]0, 1[ because the sigmoid function is applied at last.
					int must_be = digits[d].label == i? 1 : 0;
					
					//find the maximun response of the network
					if (network->output_layer->outputs[i] > network->output_layer->outputs[max]) 
					{
						max = i;
					}
					
					//want to raise the response of the perceptron
					if (must_be == 1 && network->output_layer->outputs[i] <= backprop_threshold)
					{
						network->output_layer->gradients[i] = 1;
						to_backward = 1;
					}
					//want to lower the response of the perceptron
					if (must_be == 0 && network->output_layer->outputs[i] >= backprop_threshold_inv)
					{
						network->output_layer->gradients[i] = -1;
						to_backward = 1;
					}
					
					//compute cost function
					cost += (network->output_layer->outputs[i] - must_be) * (network->output_layer->outputs[i] - must_be);
				}
				//network response is wrong
				if (max == digits[d].label) 
				{
					passed_train++;
				}
				
				//backpropagation and SGD
				if (to_backward)
				{
					backward_dnn(network, h, regulation_force);
					backprop++;
				}
				total_exed++;
			}
			kepp_running = 0;
		}
		totalTimeAffine += subTotalTimeAffine;
		
		//merge
        totalTimeCloneMerge += merge_dnn(network, nets, parallel_networks);
		double subtstop = hpc_gettime();
		totalTime += subtstop - subtstart;
		passed_test = 0;
		double testCost = 0;
		
		//check test set succes
		for(int a = 0; a < testDigitsCount; a++)
		{
			pulse_dnn_parallel(network, testDigits[a].data);
			int max = 0;
			for (int i = 1; i < OUTPUT_LAYER_SIZE; i++)
			{
				max = (network->output_layer->outputs[i] > network->output_layer->outputs[max]) ? i : max;
				int must_be = testDigits[a].label == i? 1 : 0;
				testCost += (network->output_layer->outputs[i] - must_be) * (network->output_layer->outputs[i] - must_be);
			}
			passed_test += (max == testDigits[a].label) ? 1 : 0;
		}
		
		cost /= epoch_size;
		testCost /= testDigitsCount;
		remaining_exec = (remaining_exec + epoch_size) - total_exed;
		if(graph)
		{
			//print log
			printf("%04d\t%.5f\t%.5f\t%.2f\t%.2f\n", e + 1, cost, testCost, 100 * passed_train / ((float)total_exed), 100 * passed_test / ((float)testDigitsCount));
		}
		else
		{
			//print log
			printf("E %04d/%d | C: %.5f | Ct: %.5f | backp: %.1f%% | TRAIN OK: %.2f%%, TEST OK: %.2f%% | 1/h: %.1f -> %.1f   [%.1f min] %d %d\n", e + 1, epochs, cost, testCost, 100 * backprop / ((float)total_exed) ,100 * passed_train / ((float)total_exed), 100 * passed_test / ((float)testDigitsCount), 1 / h, 1 / (final_h * ((float)e + 1) / epochs + start_h * (epochs - ((float)e + 1)) / epochs), (subtstop - subtstart) * (epochs - e - 1) / 60.0f, total_exed, remaining_exec);
		}
		fflush(stdout);
		
		//update parameters for next iteration
		h = final_h * ((float)e + 1) / epochs + start_h * (epochs - ((float)e + 1)) / epochs;
		regulation_force = (1 - (h * lamda) / digitsCount);
		passed_train = 0;
		backprop = 0;
		total_exed = 0;
		
    }
    printf("Training finished in %f s. Clone and merge time: %f s. Affine time: %f\n", totalTime, totalTimeCloneMerge, totalTimeAffine); 

	//test on test-set
    passed_test = 0;
    for(int a = 0; a < testDigitsCount; a++)
    {
		pulse_dnn(network, testDigits[a].data);
        int max = 0;
        for (int i = 1; i < OUTPUT_LAYER_SIZE; i++)
            max = (network->output_layer->outputs[i] > network->output_layer->outputs[max]) ? i : max;
		passed_test += (max == testDigits[a].label) ? 1 : 0;
    }
    printf("Correct: %d / %d\n", passed_test, testDigitsCount);
    return 0;
    
}










