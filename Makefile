EXE_DNN=mnist-dnn
CFLAGS+=-std=c99 -Wall -Wpedantic -O3

ALL: dnn

$(EXE_DNN): CFLAGS+=-fopenmp
$(EXE_DNN): LDLIBS+=-lgomp -lm

ifeq ($(ARM), 1)
  $(EXE_DNN): CFLAGS+=-mfpu=neon
endif

dnn: $(EXE_DNN)

